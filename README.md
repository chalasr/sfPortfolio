Portfolio App - Made with Symfony 2.3
================

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/4fbc5334-d1e5-4b41-bd3b-b749b18170c8/mini.png)](https://insight.sensiolabs.com/projects/4fbc5334-d1e5-4b41-bd3b-b749b18170c8)
[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/chalasr/Symfony2_Portfolio)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/chalasr/Symfony2_Portfolio/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/chalasr/Symfony2_Portfolio/?branch=master)

This application was created by Robin CHALAS - FullStack Web Developer -  http://www.chalasdev.fr/

Problems? Issues?
--------------

Write a message on chalasdev.fr or create an issue

This application requires:
-------------

- PHP >= 5.4
- Composer
- Npm

Getting Started
---------------

 - Clone this repository

 ``` git clone https://github.com/chalasr/Symfony2_Portfolio.git ```

 - Install vendor using composer

 ``` composer install ```

 - Create database

 ``` doctrine:database:create ```

 - Create schema of database

 ``` doctrine:schema:create ```

 - Start server

 ``` php app/console server:run ```

Enjoy !

Credits
-------

Author : [Robin Chalas](http://www.chalasdev.fr/)

License
-------

Copyright (c) 2014-2015 [Robin Chalas](http://www.chaladev.fr/) [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html)
